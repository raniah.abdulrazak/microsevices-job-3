

function getUserInfo() {
    console.log("User");

    fetch('https://jsonplaceholder.typicode.com/users')
        .then(response => response.json())
        .then(function (data) {
            console.log(data);
            displayUserInfo(data);
        }
    );
}


function displayUserInfo(data) {
    var table = document.getElementById("userInfoTable");
    var cols = ["id", "name", "username", "email"];
   

    // Traversing the JSON data
    for (var i = 0; i < data.length; i++) {

        var newRow = table.insertRow();
        for (var colIndex = 0; colIndex < cols.length; colIndex++) {
            var newCell = newRow.insertCell();

            // Append a text node to the cell
            var newText = document.createTextNode(data[i][cols[colIndex]]);
            newCell.appendChild(newText);}
    }
}

function getBookingInfo() {
    console.log("Booking");

    const myInit = {
        method: 'GET',
        mode: 'cors',
      };

    fetch('http://localhost:5000/api/Bookings',myInit)
        .then(response => response.json())
        .then(function(data) {
            console.log (data);
            document.getElementById("booking").innerHTML="See console for booking info";
        });
}


